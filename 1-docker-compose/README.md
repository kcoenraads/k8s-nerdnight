# hello world

## create venv
python -m venv venv

### start virtual environment
. ./venv/bin/activate

## Starten app
export FLASK_APP=main.py  
flask run
